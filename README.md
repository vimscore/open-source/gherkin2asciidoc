# A tool to convert Gherkin files to Asciidoc

See [doc/examples](doc/examples) for examples and [doc/man](doc/man) for man pages.

# Development

## Running tests

    go test
  
If a test fails after running, but the output adoc is out of date, run

    go test -update

## Building

To configure your environment, source `settings.sh`:

    . .settings.sh

It mainly adds `bin/` to path which includes some strategic wrapper scripts.

Normal development builds:

    goreleaser build --snapshot --rm-dist --single-target

### Releasing

Tag the build by hand, configure Gitlab token for uploading artifacts and let goreleaser do the rest.

    git tag vX.Y.Z
    export GITLAB_TOKEN=...
    goreleaser release --rm-dist
    sudo rm -rf dist-root && \
      docker run -e GITLAB_TOKEN --rm -it \
      -w /go/src \
      -v $(pwd):/go/src \
      -v $(pwd)/dist-root:/go/src/dist \
      golang:1.17-buster bin/goreleaser release
