package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/vimscore/open-source/gherkin2asciidoc/cmd/_internal"
	_ "gitlab.com/vimscore/open-source/gherkin2asciidoc/cmd/convert"
)

func RootCmd() *cobra.Command {
	return _internal.RootCmd
}
