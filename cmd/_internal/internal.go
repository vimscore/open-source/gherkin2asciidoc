package _internal

import (
	"fmt"
	"github.com/spf13/cobra"
	"io/ioutil"
	"log"
	"os"
)

const CmdName = "gherkin2asciidoc"

var RootCmd = &cobra.Command{Use: CmdName, SilenceUsage: true}
var Args RootArgs

type RootArgs struct {
	Verbose bool
}

var completionCmd = &cobra.Command{
	Use:   "completion",
	Short: "Generates bash completion scripts",
	Long: `To load completion run

. <(` + CmdName + ` completion)

To configure your bash shell to load completions for each session add to your bashrc

# ~/.bashrc or ~/.profile
. <(` + CmdName + ` completion)
`,
	Run: func(cmd *cobra.Command, args []string) {
		err := RootCmd.GenBashCompletion(os.Stdout)
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "%v", err)
		}
	},
}

func Init(version, date, commit string) {
	var v string
	//goland:noinspection GoBoolExpressions
	if len(version) > 0 {
		v = version
	} else {
		v = "local"

		if len(date) > 0 {
			v += ", timestamp=" + date
		}

		if len(commit) > 0 {
			v += ", git=" + commit
		}
	}

	RootCmd.Version = v

	RootCmd.PersistentFlags().BoolVarP(&Args.Verbose, "verbose", "v", false,
		"Enabled verbose output")

	RootCmd.Flags().SortFlags = false

	RootCmd.AddCommand(completionCmd)
}

func InitGlobals() error {
	if !Args.Verbose {
		log.SetOutput(ioutil.Discard)
	}

	return nil
}
