package convert

import (
	"bufio"
	"fmt"
	"github.com/cucumber/common/gherkin/go/v22"
	"github.com/cucumber/common/messages/go/v17"
	"io"
	"log"
	"math"
	"strings"
)

type Args struct {
	Hugo bool
	Toc  bool
}

type converter struct {
	args      Args
	inputName string
	reader    io.Reader
	writer    io.Writer

	headerLevel int
}

func (c *converter) convert() error {
	d, err := gherkin.ParseGherkinDocument(c.reader, func() string { return c.inputName })
	if err != nil {
		return err
	}

	if d.Feature != nil {
		c.writeFeature(d.Feature)
	}

	return nil
}

func (c *converter) Header(format string, a ...interface{}) {
	main := fmt.Sprintf(format, a...)

	c.Writeln("%s %s", strings.Repeat("=", c.headerLevel), main)
}

func (c *converter) Writeln(format string, a ...interface{}) {
	s := fmt.Sprintf(format, a...)
	s = strings.TrimRight(s, " ")
	if _, err := io.WriteString(c.writer, s); err != nil {
		log.Panicln(err)
	}
	if _, err := io.WriteString(c.writer, "\n"); err != nil {
		log.Panicln(err)
	}
}

func (c *converter) writeFeature(feature *messages.Feature) {
	if c.args.Hugo {
		c.Writeln("---")
		c.Writeln("title: %s", feature.Name)
		c.Writeln("---")
		c.Writeln("")
	}

	if c.args.Toc {
		c.Writeln(":toc:")
		c.Writeln("")
	}

	c.Header("Feature: %s", feature.Name)
	c.headerLevel++
	c.Writeln("")

	if feature.Description != "" {
		c.Writeln(c.trim(feature.Description))
		c.Writeln("")
	}

	for _, child := range feature.Children {
		c.writeFeatureChild(child)
	}
}

func (c *converter) writeFeatureChild(child *messages.FeatureChild) {
	if child == nil {
		return
	}

	if child.Background != nil {
		c.Header("Background: %s", child.Background.Name)
		c.Writeln(c.trim(child.Background.Description))
		c.writeSteps(child.Background.Steps)
	}

	if child.Rule != nil {
		c.writeRule(child.Rule)
	}

	c.writeScenario(child.Scenario)
}

func (c *converter) writeScenario(scenario *messages.Scenario) {
	if scenario == nil {
		return
	}

	c.Header("Scenario: %s", strings.TrimSpace(scenario.Name))
	c.headerLevel++
	c.Writeln("")

	if c.writeDescription(scenario.Description) {
		c.Header("Steps")
		c.Writeln("")
		c.AddHeader(1)
		defer c.AddHeader(-1)
	}

	c.writeSteps(scenario.Steps)

	for _, example := range scenario.Examples {
		c.Header("Example: " + strings.TrimSpace(example.Name))
		c.writeDescription(example.Description)
		c.writeRows(example.TableHeader, example.TableBody)
		c.Writeln("")
	}

	c.headerLevel--
}

func (c *converter) writeRule(rule *messages.Rule) {
	c.Header("Rule: %s", strings.TrimSpace(rule.Name))
	c.Writeln("")
	c.headerLevel++

	if c.writeDescription(rule.Description) {
		//c.AddHeader(1)
		//defer c.AddHeader(-1)
	}

	for _, child := range rule.Children {
		if child.Background != nil {
			c.Header("Background: %s", child.Background.Name)
			c.Writeln(c.trim(child.Background.Description))
			c.writeSteps(child.Background.Steps)
		}

		c.writeScenario(child.Scenario)
	}
	c.headerLevel--
}

func (c *converter) writeSteps(steps []*messages.Step) {
	for _, step := range steps {
		kw := strings.TrimSpace(step.Keyword)
		c.Writeln("%s %s", strings.TrimSpace(kw), strings.TrimSpace(step.Text))

		c.writeDocString(step.DocString)
		c.writeDataTable(step.DataTable)
		c.Writeln("")
	}
}

func (c *converter) writeDocString(docString *messages.DocString) {
	if docString == nil {
		return
	}

	if docString.MediaType == "" || docString.MediaType == "text/plain" {
		s := "> " + docString.Content
		s = strings.TrimSpace(s)
		s = strings.ReplaceAll(s, "\n", " +\n> ")
		c.Writeln(s)
	} else {
		c.Writeln("Unsupported docstring!")
		c.Writeln("Media type: " + docString.MediaType)
	}
}

func (c *converter) writeDataTable(table *messages.DataTable) {
	if table == nil {
		return
	}

	c.writeRows(nil, table.Rows)
}

func (c *converter) writeRows(header *messages.TableRow, rows []*messages.TableRow) {
	c.Writeln("")

	cols := 0
	for _, row := range rows {
		if n := len(row.Cells); n > cols {
			cols = n
		}
	}

	c.Writeln("")
	c.Writeln("[cols=\"%d\"]", cols)
	c.Writeln("[%%autowidth]")
	/*
		c.Write("[cols=\"")
		for i := 0; i < cols; i++ {
			if i > 0 {
				c.Write(",")
			}
			c.Write("1")
		}
		c.Writeln("\"]")
	*/
	if header != nil {
		c.Writeln("[%%header]")
	}

	c.Writeln("|===")
	if header != nil {
		for _, cell := range header.Cells {
			c.Writeln("| %s", cell.Value)
		}

		c.Writeln("")
	}

	for _, row := range rows {
		//if i == 0 {
		//	c.Writeln("|===")
		//} else {
		//	c.Writeln("")
		//}

		for _, cell := range row.Cells {
			c.Writeln("| %s", cell.Value)
		}
	}
	c.Writeln("|===")
}

func (c *converter) writeDescription(description string) bool {
	description = c.trimDocstring(description)
	if len(description) == 0 {
		return false
	}

	c.Writeln(description)
	c.Writeln("")

	return true
}

/// A very complicated way of removing leading space
func (c *converter) trim(s string) string {

	scanner := bufio.NewScanner(strings.NewReader(strings.TrimSpace(s)))

	scanner.Split(bufio.ScanLines)

	var lines []string

	prefix := math.MaxInt32

	for i := 0; scanner.Scan(); i++ {
		line := scanner.Text()
		trimmed := strings.TrimSpace(line)

		if len(trimmed) == 0 {
			lines = append(lines, line)
			continue
		}

		lines = append(lines, line)
		delta := len(line) - len(trimmed)
		if delta > 0 && delta < prefix {
			prefix = delta
		}
	}

	if prefix == math.MaxInt32 {
		prefix = 0
	}

	out := ""
	for i, line := range lines {
		s := line
		if len(line) > 1 && line[0] == ' ' {
			s = line[prefix:]
		}

		if i > 0 {
			out += "\n"
		}

		s = strings.TrimRight(s, " ")

		out += s
	}

	return out
}

func (c *converter) trimDocstring(docstring string) string {
	docstring = strings.TrimSpace(docstring)
	if len(docstring) < 6 {
		return docstring // This shouldn't happen
	}

	chr := docstring[0:1]

	docstring = strings.TrimLeft(docstring, chr)
	docstring = strings.TrimRight(docstring, chr)
	return c.trim(docstring)
}

func (c *converter) AddHeader(delta int) {
	c.headerLevel += delta
}

func Run(args *Args, reader io.Reader, writer io.Writer) error {
	c := converter{
		args:   *args,
		reader: reader,
		writer: writer,

		headerLevel: 1,
	}
	return c.convert()
}
