package convert

import (
	"github.com/spf13/cobra"
	"gitlab.com/vimscore/open-source/gherkin2asciidoc/cmd/_internal"
	"os"
)

var convert = struct {
	input string
	args  Args
}{}

func runConvert(*cobra.Command, []string) error {
	err := _internal.InitGlobals()
	if err != nil {
		return err
	}

	f, err := os.Open(convert.input)
	if err != nil {
		return err
	}
	defer func() { _ = f.Close() }()

	return Run(&convert.args, f, os.Stdout)
}

func init() {
	c := &cobra.Command{
		Use:   "convert",
		RunE:  runConvert,
		Short: "Converts a Gherkin file to Asciidoc",
		Long: `To convert a file run

    gherkin2asciidoc convert --input feature.gherkin > feature.adoc
`,
	}
	_internal.RootCmd.AddCommand(c)

	c.Flags().StringVar(&convert.input, "input", "", "File to convert")
	_ = c.MarkFlagRequired("input")

	c.Flags().BoolVar(&convert.args.Hugo, "hugo", false, "Hugo mode")
	c.Flags().BoolVar(&convert.args.Toc, "toc", false, "Emit :toc: in generated files")
}
