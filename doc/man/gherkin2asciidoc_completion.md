## gherkin2asciidoc completion

Generates bash completion scripts

### Synopsis

To load completion run

. <(gherkin2asciidoc completion)

To configure your bash shell to load completions for each session add to your bashrc

# ~/.bashrc or ~/.profile
. <(gherkin2asciidoc completion)


```
gherkin2asciidoc completion [flags]
```

### Options

```
  -h, --help   help for completion
```

### Options inherited from parent commands

```
  -v, --verbose   Enabled verbose output
```

### SEE ALSO

* [gherkin2asciidoc](gherkin2asciidoc.md)	 - 

