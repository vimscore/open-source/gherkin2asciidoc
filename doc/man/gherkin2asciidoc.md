## gherkin2asciidoc



### Options

```
  -v, --verbose   Enabled verbose output
  -h, --help      help for gherkin2asciidoc
```

### SEE ALSO

* [gherkin2asciidoc completion](gherkin2asciidoc_completion.md)	 - Generates bash completion scripts
* [gherkin2asciidoc convert](gherkin2asciidoc_convert.md)	 - Converts a Gherkin file to Asciidoc

