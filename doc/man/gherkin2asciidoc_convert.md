## gherkin2asciidoc convert

Converts a Gherkin file to Asciidoc

### Synopsis

To convert a file run

    gherkin2asciidoc convert --input feature.gherkin > feature.adoc


```
gherkin2asciidoc convert [flags]
```

### Options

```
  -h, --help           help for convert
      --hugo           Hugo mode
      --input string   File to convert
      --toc            Emit :toc: in generated files
```

### Options inherited from parent commands

```
  -v, --verbose   Enabled verbose output
```

### SEE ALSO

* [gherkin2asciidoc](gherkin2asciidoc.md)	 - 

