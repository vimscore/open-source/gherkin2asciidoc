BIN=gherkin2asciidoc
GOSRC=$(shell find . -name \*.go)
export MAKEFLAGS=-r

all: $(BIN) test
.PHONY: all

bin: $(BIN)
.PHONY: bin

$(BIN): $(GOSRC)
	go build -o $(BIN) .

test:
	go test .

clean:
	rm -f $(wildcard $(BIN)*)
