module gitlab.com/vimscore/open-source/gherkin2asciidoc

go 1.15

require (
	github.com/cucumber/common/gherkin/go/v22 v22.0.0
	github.com/cucumber/common/messages/go/v17 v17.1.1
	github.com/sebdah/goldie/v2 v2.5.3
	github.com/spf13/cobra v1.2.1
)
