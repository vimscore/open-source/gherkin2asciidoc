package main

import (
	"bytes"
	"github.com/sebdah/goldie/v2"
	"gitlab.com/vimscore/open-source/gherkin2asciidoc/cmd/_internal"
	"gitlab.com/vimscore/open-source/gherkin2asciidoc/cmd/convert"
	"os"
	"testing"
)

func doTest(t *testing.T, g *goldie.Goldie, test *TestFile) {
	f, err := os.Open("doc/examples/" + test.name + ".feature")
	if err != nil {
		t.Error(err)
		return
	}
	defer func() { _ = f.Close() }()

	var output bytes.Buffer
	if err = convert.Run(&test.args, f, &output); err != nil {
		t.Error(err)
	}
	g.Assert(t, test.name, output.Bytes())
}

type TestFile struct {
	name string
	args convert.Args
}

func Test_Convert(t *testing.T) {
	g := goldie.New(t,
		goldie.WithFixtureDir("doc/examples"),
		goldie.WithNameSuffix(".adoc"),
	)

	_internal.Args.Verbose = testing.Verbose()
	if err := _internal.InitGlobals(); err != nil {
		t.Error(err)
	}

	args := convert.Args{}
	argsHugoToc := convert.Args{Hugo: true, Toc: true}

	testFiles := []TestFile{
		{"feature1", args},
		{"feature2", args},
		{"feature3", args},
		{"feature4", args},
		{"feature5", argsHugoToc},
		{"feature6", args},
	}

	for _, file := range testFiles {
		t.Run(file.name, func(t *testing.T) {
			doTest(t, g, &file)
		})
	}
}
