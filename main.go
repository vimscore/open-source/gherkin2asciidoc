package main

import (
	"gitlab.com/vimscore/open-source/gherkin2asciidoc/cmd"
	"gitlab.com/vimscore/open-source/gherkin2asciidoc/cmd/_internal"
	"os"
)

var version, date, commit string

func main() {
	_internal.Init(version, date, commit)
	if err := cmd.RootCmd().Execute(); err != nil {
		os.Exit(1)
	}
}
